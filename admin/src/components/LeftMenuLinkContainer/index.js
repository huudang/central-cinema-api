import React from 'react'
import { useLocation } from 'react-router-dom'
import PropTypes from 'prop-types'
import { get, snakeCase, isEmpty } from 'lodash'

import { SETTINGS_BASE_URL } from '../../config'
import Wrapper from './Wrapper'
import messages from './messages.json'

import LeftMenuLinkSection from '../LeftMenuLinkSection'

const LeftMenuLinkContainer = ({ plugins }) => {
  const location = useLocation()

  // Generate the list of content types sections
  const contentTypesSections = Object.keys(plugins).reduce((acc, current) => {
    plugins[current].leftMenuSections.forEach((section = {}) => {
      if (!isEmpty(section.links)) {
        acc[snakeCase(section.name)] = {
          name: section.name,
          searchable: true,
          links: get(acc[snakeCase(section.name)], 'links', []).concat(
            section.links
              .filter(link => link.isDisplayed !== false)
              .map(link => {
                link.plugin = !isEmpty(plugins[link.plugin]) ? link.plugin : plugins[current].id
                return link
              })
          )

        }
      }
    })

    return acc
  }, {})

  const Collection = { ...contentTypesSections.collection_type }

  Collection.links = new Array([...contentTypesSections.collection_type.links])
  for (let index = 0; index < Collection.links.length; index++) {
    Collection.links[index].isDisplayed = false
    if (Collection.links[index] && Collection.links[index].schema && Collection.links[index].schema.kind === 'collectionType') {
      if (Collection.links[index].name === 'tour') {
        Collection.links[index].label = 'Các chuyến đi'
        Collection.links[index].isDisplayed = true
      } else if (Collection.links[index].name === 'deposit') {
        Collection.links[index].label = 'Quản lí giao dịch'
        Collection.links[index].isDisplayed = true
      } else if (Collection.links[index].uid === 'plugins::users-permissions.user') {
        Collection.links[index].label = 'Hướng dẫn viên'
        Collection.links[index].isDisplayed = true
      } else {
        Collection.links[index].isDisplayed = false
      }
    }
  }

  Collection.name = 'Collection'
  // contentTypesSections.collection = Collection

  // Generate the list of plugin links (plugins without a mainComponent should not appear in the left menu)
  const pluginsLinks = Object.values(plugins)
    .filter(
      plugin => plugin.id !== 'email' && plugin.id !== 'content-manager' && !!plugin.mainComponent
    )
    .map(plugin => {
      const pluginSuffixUrl = plugin.suffixUrl ? plugin.suffixUrl(plugins) : ''

      return {
        icon: get(plugin, 'icon') || 'plug',
        label: get(plugin, 'name'),
        destination: `/plugins/${get(plugin, 'id')}${pluginSuffixUrl}`
      }
    })

  const menu = {
    ...contentTypesSections,
    plugins: {
      searchable: false,
      name: 'plugins',
      emptyLinksListMessage: messages.noPluginsInstalled.id,
      links: pluginsLinks
    },
    general: {
      searchable: false,
      name: 'general',
      links: [
        {
          icon: 'list',
          label: messages.listPlugins.id,
          destination: '/list-plugins'
        },
        {
          icon: 'shopping-basket',
          label: messages.installNewPlugin.id,
          destination: '/marketplace'
        },
        {
          icon: 'cog',
          label: messages.settings.id,
          destination: SETTINGS_BASE_URL
        }
      ]
    }
  }

  console.log(Collection)
  return (
    <Wrapper>
      {Object.keys(menu).map(current => {
        return (
          <LeftMenuLinkSection
            key={current}
            links={menu[current].links}
            section={current}
            location={location}
            searchable={menu[current].searchable}
            emptyLinksListMessage={menu[current].emptyLinksListMessage}
          />
        )
      })}
    </Wrapper>
  )
}

LeftMenuLinkContainer.propTypes = {
  plugins: PropTypes.object.isRequired
}

export default LeftMenuLinkContainer
