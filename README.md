## Getting Started

After clone this code, let's install modules

```bash
npm install
# or
yarn install
```

First, run the development server:

```bash
npm run develop
# or
yarn develop
```

Open [http://localhost:1337](http://localhost:3000) with your browser to see the result.

run the production server:

```bash
npm run build
# or
yarn build

```bash
npm run start
# or
yarn start
```
